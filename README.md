# HCDO AWS ASG/LC Terraform Module
## Inputs

| Name | Description | Default | Required |
|------|-------------|:-----:|:-----:|
| huit_assetid | HUIT Asset ID as assigned in Slash | - | yes |
| product | HUIT Product ID as assigned in Slash | - | yes |
| environment | Application specific environment | - | yes |
| environment_map | Application specific environment mapping that tags will be populated with | `<map>` | no |
| function | Functional Tier for Application | - | yes |
| ami_id | The AMI ID to be used in the Launch Configuration | - | yes |
| instance_type | The instance type to be used in the Launch Configuration | - | yes |
| iam_instance_profile | The IAM instance profile to be used in the Launch Configuration | - | yes |
| key_name | The SSH Key name to be used in the Launch Configuration | - | yes |
| security_group | The security group the instances to use | - | yes |
| user_data | The path to a file with user_data for the instances | - | yes |
| asg_number_of_instances | The number of instances we want in the ASG | - | yes |
| asg_minimum_number_of_instances | The minimum number of instances the ASG should maintain | `1` | no |
| health_check_grace_period | Number of seconds for a health check to time out | `300` | no |
| health_check_type | The type of health check the AutoScaling Group uses to evaluate instance health | `"EC2"` | no |
| subnet_azs | The VPC subnet IDs | - | yes |
| azs | Availability Zones | - | yes |
| aws_access_key | The AWS IAM Access Key string | - | yes |
| aws_secret_key | The AWS IAM Secret Key string | - | yes |
| aws_region | The AWS region | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| launch_config_id | Output the ID of the Launch Config |
| asg_id | Output the ID of the AutoScaling Group |

