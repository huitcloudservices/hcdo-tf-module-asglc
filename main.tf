# AWS Launch Configuration Creation
resource "aws_launch_configuration" "launch_config" {
  name = "${lower(var.product)}-${lower(var.function)}-${lower(var.environment)}-lc"
  image_id = "${var.ami_id}"
  instance_type = "${var.instance_type}"
  iam_instance_profile = "${var.iam_instance_profile}"
  key_name = "${var.key_name}"
  security_groups = ["${var.security_group}"]
  user_data = "${file(var.user_data)}"
}

# AWS AutoScaling Group Creation
resource "aws_autoscaling_group" "main_asg" {
  depends_on = ["aws_launch_configuration.launch_config"]
  name = "${lower(var.product)}-${lower(var.function)}-${lower(var.environment)}-asg"
  availability_zones = ["${split(",", var.azs)}"]
  vpc_zone_identifier = ["${split(",", var.subnet_azs)}"]
  launch_configuration = "${aws_launch_configuration.launch_config.id}"
  max_size = "${var.asg_number_of_instances}"
  min_size = "${var.asg_minimum_number_of_instances}"
  desired_capacity = "${var.asg_number_of_instances}"
  health_check_grace_period = "${var.health_check_grace_period}"
  health_check_type = "${var.health_check_type}"

  tag {
    key                 = "Name"
    value               = "${lower(var.product)}-${lower(var.function)}-${lower(var.environment)}-asg"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "huit_assetid"
    value               = "${var.huit_assetid}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "product"
    value               = "${var.product}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "environment"
    value               = "${lookup(var.environment_map, lower(var.environment))}"
    propagate_at_launch = "true"
  }
}
