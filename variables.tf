# Manditory variables for all modules
variable "huit_assetid" {
  description = "HUIT Asset ID as assigned in Slash"
}

variable "product" {
  description = "HUIT Product ID as assigned in Slash"
}

variable "environment" {
  description = "Application specific environment"
}

variable "environment_map" {
  description = "Application specific environment mapping that tags will be populated with"
  type    = "map"
  default = {
    dev   = "Development"
    test  = "Testing"
    stage = "Staging"
    prod  = "Production"
  }
}

# Required Data Providers
data "aws_availability_zones" "available" {}

variable "function" {
  description = "Functional Tier for Application"
}

variable "ami_id" {
  description = "The AMI ID to be used in the Launch Configuration"
}

variable "instance_type" {
  description = "The instance type to be used in the Launch Configuration"
}

variable "iam_instance_profile" {
  description = "The IAM instance profile to be used in the Launch Configuration"
}

variable "key_name" {
  description = "The SSH Key name to be used in the Launch Configuration"
}

variable "security_group" {
  description = "The security group the instances to use"
}

variable "user_data" {
  description = "The path to a file with user_data for the instances"
}

variable "asg_number_of_instances" {
  description = "The number of instances we want in the ASG"
  // We use this to populate the following ASG settings
  // - max_size
  // - desired_capacity
}

variable "asg_minimum_number_of_instances" {
  description = "The minimum number of instances the ASG should maintain"
  default = 1
  // Defaults to 1
  // Can be set to 0 if you never want the ASG to replace failed instances
}

variable "health_check_grace_period" {
  description = "Number of seconds for a health check to time out"
  default = 300
}

variable "health_check_type" {
  description = "The type of health check the AutoScaling Group uses to evaluate instance health"
  default = "EC2"
  //Types available are:
  // - ELB
  // - EC2
  // * http://docs.aws.amazon.com/cli/latest/reference/autoscaling/create-auto-scaling-group.html#options
}

variable "subnet_azs" {
  description = "The VPC subnet IDs"
  // comma separated list
}

variable "azs" {
  description = "Availability Zones"
  // comma separated list
}

// Variables for providers used in this module
variable "aws_access_key" {
  description = "The AWS IAM Access Key string"
}

variable "aws_secret_key" {
  description = "The AWS IAM Secret Key string"
}

variable "aws_region" {
  description = "The AWS region"
}
